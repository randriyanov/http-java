package com.ra.http.grizzly;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@PreMatching
public class PrematchingRequestFilter implements ContainerRequestFilter {


    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {
        System.out.println("prematching filter");
        if (ctx.getMethod()
            .equals("DELETE")) {
            System.out.println("Deleting request");
        }
    }
}
