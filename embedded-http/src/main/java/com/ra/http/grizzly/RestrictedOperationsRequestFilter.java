package com.ra.http.grizzly;

import com.ra.http.grizzly.config.HelloBinding;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@Priority(Priorities.AUTHORIZATION)
@HelloBinding
public class RestrictedOperationsRequestFilter implements ContainerRequestFilter {


    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {
        System.out.println("Restricted operations filter");
        if (ctx.getLanguage() != null && "EN".equals(ctx.getLanguage()
            .getLanguage())) {
            System.out.println("Aborting request");
            ctx.abortWith(Response.status(Response.Status.FORBIDDEN)
                .entity("Cannot access")
                .build());
        }

    }
}
